from kivy.app import App
from kivy.uix.widget import Widget
from kivy.lang import Builder
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.animation import Animation

kv = Builder.load_file('my_kv.kv')


class MyLayout(Widget):
    def switch_active(self, switch_object, value):
        print(value)



class AwesomeApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    AwesomeApp().run()
